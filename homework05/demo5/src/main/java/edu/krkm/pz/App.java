package edu.krkm.pz;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class App 
{
    public static void main( String[] args )
    {
        List<Integer> list = new ArrayList<Integer>(4);
        list.add(0, 10);
        list.add(1, 11);
        list.add(2, 12);
        list.add(3, 13);
        Comma_zip object = new Comma_zip();
        System.out.println(object.getString(list));
        Stream<String> stream1 = Stream.of("a", "b", "c", "d", "e");
        Stream<String> stream2 = Stream.of("x", "y", "z");
        Stream<String> zippedStream = Comma_zip.zip(stream1, stream2);

        zippedStream.forEach(System.out::print);

        System.out.println();
    }
}
