package edu.krkm.pz;

import java.util.Iterator;
import java.util.stream.Stream;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Stream.concat;

import java.util.List;

public class Comma_zip{

    public Comma_zip() {}

    public String getString(List<Integer> list) {
        return list.stream()
                .map(i -> i % 2 == 0 ? "e" + i : "o" + i)
                .collect(joining(","));
    }

    public static <T> Stream<T> zip(Stream<T> first, Stream<T> second) {
        Iterator<T> it1 = first.iterator();
        Iterator<T> it2 = second.iterator();
        Stream<T> zipStream = Stream.empty();

        while (it1.hasNext() && it2.hasNext()) {
            zipStream = concat(zipStream, Stream.of(it1.next(), it2.next()));
        }

        return zipStream;
    }
}