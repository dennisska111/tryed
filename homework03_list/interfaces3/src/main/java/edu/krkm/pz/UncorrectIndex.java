package edu.krkm.pz;

public class UncorrectIndex extends Exception {
    public UncorrectIndex(String message) {
        super(message);
    }

    public UncorrectIndex() {
        super();
    }

    public UncorrectIndex(String message, Throwable cause, int errorCode) {
        super(message, cause);
    }
}
