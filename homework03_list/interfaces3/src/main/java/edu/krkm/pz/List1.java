package edu.krkm.pz;

public interface List1 <T>{

    void view();
    void set(int index, T info) throws UncorrectIndex;
    void seek(int index) throws UncorrectIndex;

    void addStart(T info);
    void addMiddle(int index ,T info) throws UncorrectIndex;
    void addEnd(T info);

    T delStart();
    T delMiddle(int index);
    T delEnd();
    T delIndex(int index) throws UncorrectIndex;

    void setSize(int newSize);
    int getSize();
    T getInfo(int index);
}



