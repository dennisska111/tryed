package edu.krkm.pz;

import org.junit.*;
import org.junit.rules.ExpectedException;
import java.util.Scanner;

public class ListmplTest
{
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    Scanner in = new Scanner(System.in);
    Listmpl<Integer> list = new Listmpl<Integer>(4);


    @Before
    public void setSize() throws UncorrectIndex {
        list.setSize(4);
        list.set(1, 5);
        list.set(2, 6);
        list.set(3, 7);
        list.set(4, 8);
    }

    @Test
    public void getSize(){
        //GIVEN
        int sizeListSame = 4;

        //WHEN
        int sizeList =  list.getSize();

        //THEN
        Assert.assertEquals(sizeListSame, sizeList);
    }

    @Test
    public void set() throws UncorrectIndex{
        //GIVEN
        Integer info = 8;
        Integer index = 4;
        int expectedSize = 4;
        //WHEN
        list.set(4,8);
        int resultSize = list.getSize();
        //THEN
        Assert.assertEquals(info, list.getInfo(index - 1));
        Assert.assertEquals(expectedSize, resultSize);
    }

    @Test
    public void setWithException() throws UncorrectIndex{
        //GIVEN
        Integer info = 8;
        Integer index = 5;
        int NowSize = list.getSize();

        //WHEN
        expectedException.expect(UncorrectIndex.class);
        expectedException.expectMessage("Ви вийшли за межі списку, такого номеру немає! " + index);
        list.set(index, info);
    }

    @Test
    public void addStart(){
        //GIVEN
        Integer expected = 5;
        int initialSize = list.getSize();

        //WHEN
        list.addStart(expected);
        int sizeNew = list.getSize();

        //THEN
        Assert.assertEquals(expected, list.getInfo(0));
        Assert.assertTrue(sizeNew - initialSize == 1);
    }

    @Test
    public void addMiddle() throws UncorrectIndex{
        //GIVEN
        Integer expectedInfo = 7;
        Integer index = 2;
        int initialSize = list.getSize();

        //WHEN
        list.addMiddle(index, expectedInfo);
        int sizeNew = list.getSize();

        //THEN
        Assert.assertEquals(expectedInfo, list.getInfo(index-1));
        Assert.assertTrue(sizeNew - initialSize ==1);
    }

    @Test
    public void addMiddleWithException() throws UncorrectIndex {
        //given
        Integer expected = 7;
        Integer index = -5;
        int initialSize = list.getSize();

        //WHEN
        expectedException.expect(UncorrectIndex.class);
        expectedException.expectMessage("Ви вийшли за межі списку, такого номеру немає! " + index);
        list.addMiddle(index, expected);
    }

    @Test
    public void addEnd(){
        //GIVEN
        Integer expectedInfo = 123;
        int initialSize = list.getSize();

        //WHEN
        list.addEnd(expectedInfo);
        int index = list.getSize();

        //THEN
        Assert.assertEquals(expectedInfo, list.getInfo(index-1));
        Assert.assertTrue(index - initialSize == 1);
    }
    @Test
    public void delStart(){
        //GIVEN
        int initialSize = list.getSize();

        //WHEN
        Integer delValue = list.delStart();
        int sizeNew = list.getSize();

        //THEN
        Assert.assertNotEquals(delValue, list.getInfo(1));
        Assert.assertTrue( initialSize - sizeNew == 1);
    }

    @Test
    public void delMiddle(){
        // GIVEN
        int index = 2;
        int initialSize = list.getSize();

        //WHEN
        Integer delValueIndex = list.delMiddle(index);
        int sizeNew = list.getSize();

        //THEN
        Assert.assertNotEquals(delValueIndex, list.getInfo(index));
        Assert.assertTrue(initialSize - sizeNew == 1);
    }

    @Test
    public void delIndex() throws UncorrectIndex{
        // GIVEN
        int index = 2;
        int initialSize = list.getSize();

        //WHEN
        Integer delValueIndex = list.delIndex(index);
        int sizeNew = list.getSize();

        //THEN
        Assert.assertNotEquals(delValueIndex, list.getInfo(index));
        Assert.assertTrue(initialSize - sizeNew == 1);
    }

    @Test
    public void delIndexWithException() throws UncorrectIndex{
        // GIVEN
        int index = -2;
        int initialSize = list.getSize();

        //WHEN
        expectedException.expect(UncorrectIndex.class);
        expectedException.expectMessage("Ви вийшли за межі списку, такого номеру немає! " + index);
        Integer delValueIndex = list.delIndex(index);
    }

    @Test
    public void delEnd(){
        //THEN
        int initialSize = list.getSize();

        //WHEN
        Integer delInfoLast = list.delEnd();
        int sizeNew = list.getSize();

        //THEN
        Assert.assertNotEquals(delInfoLast, list.getInfo(sizeNew - 1));
        Assert.assertTrue(initialSize - sizeNew == 1);
    }

    @Test
    public void getInfo(){
        //GIVEN
        Integer expectedInfo = 6;

        //WHEN
        Integer value = list.getInfo(1);

        //THEN
        Assert.assertEquals(expectedInfo, value);
    }
}
