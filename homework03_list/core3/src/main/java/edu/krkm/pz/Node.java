package edu.krkm.pz;

public class Node<T> {
    public T info;
    public Node next;

    public Node() {
        info = null;
        next = null;
    }

    public Node(T newInfo) {
        info = newInfo;
        next = null;
    }
}
