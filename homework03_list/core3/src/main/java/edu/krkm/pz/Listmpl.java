package edu.krkm.pz;

import org.w3c.dom.ls.LSOutput;

import java.util.Scanner;


public class Listmpl<T> extends Node<T> implements List1<T> {

    private static class Validator {
        public static void validateIndex(int index, int size) throws UncorrectIndex {
            if (index < 1 || index > size) {
                throw new UncorrectIndex("Ви вийшли за межі списку, такого номеру немає! " + index);
            }
        }
    }

    Node<T> head;
    Node<T> tail;
    private int size;

    public Listmpl() {
        create(6);
    }

    public Listmpl(int newSize) {
        create(newSize);
    }

    private void create(int newSize) {
        size = newSize;
        head = new Node<T>();
        Node<T> newTail = head;
        tail = head;
        int kol = 1;
        while (kol < size) {
            newTail.next = new Node<T>();
            newTail = newTail.next;
            if (kol == size - 1) tail = newTail;
            kol++;
        }
    }

    @Override
    public void view() {
        System.out.println("Список: ");
        for (int i = 0; i < getSize(); i++)
            System.out.println("(" + (i + 1) + ") - елемент " + getInfo(i));

    }

    @Override
    public void set(int index, T info) throws UncorrectIndex {
        Validator.validateIndex(index, size);
        index--;
        Node<T> newHead = head;
        for (int i = 0; i < index; ++i) {
            newHead = newHead.next;
        }
        newHead.info = info;
    }

    @Override
    public void seek(int index) throws UncorrectIndex {
        Validator.validateIndex(index, size);
        index--;
        Node<T> newHead = head;
        for (int i = 0; i < index; ++i) {
            newHead = newHead.next;
        }
        index++;
        System.out.println("Пошук по номеру (" + index + ") = " + newHead.info);
    }

    @Override
    public void addStart(T info) {
        Node<T> newHead = new Node<T>(info);
        newHead.next = head;
        head = newHead;
        size++;
    }

    @Override
    public void addMiddle(int index, T info) throws UncorrectIndex {
        Validator.validateIndex(index, size);
        int i;
        index--;
        Node<T> newTail = head;
        for (i = 0; i < index - 1; i++) {
            newTail = newTail.next;
        }
        Node<T> new_Node = new Node<T>(info);
        new_Node.next = newTail.next;
        newTail.next = new_Node;
        size++;
    }

    @Override
    public void addEnd(T info) {
        Node<T> newTail = new Node<T>(info);
        tail.next = newTail;
        tail = newTail;
        size++;
    }

    @Override
    public T delStart() {
        T delValue = head.info;
        head = head.next;
        size--;
        return delValue;
    }

    @Override
    public T delMiddle(int index) {
        T newValue;
        Node<T> newHead = head;
        Node<T> temp;
        for (int i = 0; i < size - 1; i++) {
            if (i == index - 1) break;
            else newHead = newHead.next;
        }
        temp = newHead.next;
        newValue = temp.info;
        newHead.next = newHead.next.next;
        size--;
        return newValue;
    }

    @Override
    public T delEnd() {
        Node<T> headNew = head;
        T del_value = tail.info;
        while (headNew.next.next != null) {
            headNew = headNew.next;
        }
        tail = headNew;
        tail.next = null;
        size--;
        return del_value;
    }

    @Override
    public T delIndex(int index) throws UncorrectIndex {
        Validator.validateIndex(index, size);
        index--;
        T newValue;
        int poz;
        poz = 3;
        if (index == 0) poz = 1;
        if (index == size - 1) poz = 2;
        switch (poz) {
            case 1:
                newValue = delStart();
            case 2:
                newValue = delEnd();
            default: {
                newValue = delMiddle(index);
            }
        }
        return newValue;
    }


    @Override
    public void setSize(int newSize) {
        size = newSize;
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public T getInfo(int index) {
        int i;
        Node<T> newHead = head;
        for (i = 0; i < index; ++i) {
            newHead = newHead.next;
        }
        return newHead.info;
    }
}