package edu.krkm.pz;
import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Введіть розмір списку до 6 елем: " );
        Scanner in = new Scanner(System.in);
        int size = in.nextInt();
        Listmpl<Integer> list = new Listmpl<Integer>(size);
        list.setSize(size);
        System.out.println("Розмір списку: " + list.getSize());

        System.out.println("\n");
        for (int i = 0; i < size; i++) {
            System.out.println("Введіть " + (i + 1) + "-ий елемент списку: ");
            try {
                list.set( i + 1, in.nextInt());
            } catch (UncorrectIndex e) {
                System.out.println(e.getMessage());
            }

        }
        System.out.println("\n");

        list.view();

        System.out.println("\n");

        System.out.println("Додавання в середину");
        System.out.println("Введіть номер: ");
        int nomer = in.nextInt();
        System.out.println("Введіть елемент: ");
        int elem = in.nextInt();
        try {
            list.addMiddle(nomer, elem);
        } catch (UncorrectIndex e) {
            System.out.println(e.getMessage());
        }
        list.view();

        System.out.println("\n");

        System.out.println("Додавання в початок");
        System.out.println("Введіть елемент: ");
        elem = in.nextInt();
        list.addStart(elem);
        list.view();

        System.out.println("\n");

        System.out.println("Додавання в кінець");
        System.out.println("Введіть елемент: ");
        elem = in.nextInt();
        list.addEnd(elem);
        list.view();

        System.out.println("\n");

        System.out.println("Розмір списку: " + list.getSize());

        System.out.println("\n");

        System.out.println("Видалення по номеру");
        System.out.println("Введіть номер: ");
        nomer = in.nextInt();
        try {
            Integer rez = list.delIndex(nomer);
            System.out.println("Видалений елемент =  " + rez);
        } catch (UncorrectIndex e) {
            System.out.println(e.getMessage());
        }
        list.view();

        System.out.println("\n");

        System.out.println("Розмір списку: " + list.getSize());

        System.out.println("\n");

        System.out.println("Введіть номер для пошуку його елементу: ");
        nomer = in.nextInt();
        try {
            list.seek(nomer);
        } catch (UncorrectIndex uncorrectIndex) {
            uncorrectIndex.printStackTrace();
        }

        System.out.println("\n");

        System.out.println("Видалення першого елементу списку");
        Integer del1 = list.delStart();
        System.out.println("Видалений елемент =  " + del1);
        list.view();

        System.out.println("\n");

        System.out.println("Видалення останнього елементу списку.");
        Integer del2 = list.delEnd();
        System.out.println("Видалене число =  " + del2);
        list.view();

        System.out.println("\n");

        System.out.println("Розмір списку: " + list.getSize());

    }
}
