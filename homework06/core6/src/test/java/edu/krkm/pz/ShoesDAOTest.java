package edu.krkm.pz;

import org.junit.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;

import static org.junit.Assert.*;

public class ShoesDAOTest {

    ShoesDAO shoesDAO = new ShoesDAO();

    @BeforeClass
    public static void initialise() throws IOException, SQLException {
        ShopDAO shopDAO = new ShopDAO();
        ShopEntity startShop1 = new ShopEntity("ATB", "Dnipro");
        startShop1.setNumber(1);
        shopDAO.addEntity(startShop1);
    }

    @AfterClass
    public static void clearTableShop() throws IOException, SQLException {
        ShopDAO shopDAO = new ShopDAO();
        shopDAO.deleteEntity(1);
        shopDAO.deleteEntity(2);
    }

    @After
    public void clearTableShoes() throws IOException, SQLException {
        for(ShoesEntity shoes: shoesDAO.getEntities()) {
            shoesDAO.deleteEntity(shoes.getShoesNumber());
        }
    }

    @Test
    public void addEntity() throws IOException, SQLException {
        //GIVEN
        ShoesEntity expectedShoes = new ShoesEntity(1, "Slaps", 250);
        expectedShoes.setShoesNumber(10);

        //WHEN
        shoesDAO.addEntity(expectedShoes);
        ShoesEntity resultShoes = shoesDAO.getEntities().getFirst();

        //THEN
        assertEquals(expectedShoes, resultShoes);
    }

    @Test
    public void addEntitiesList() throws IOException, SQLException {
        //GIVEN
        ShoesEntity expectedShoesFirst = new ShoesEntity(1, "Slaps", 250);
        expectedShoesFirst.setShoesNumber(11);
        ShoesEntity expectedShoesSecond = new ShoesEntity(1, "Boots", 1200);
        expectedShoesSecond.setShoesNumber(12);
        LinkedList<ShoesEntity> expectedShoesList = new LinkedList<>();
        expectedShoesList.add(expectedShoesFirst);
        expectedShoesList.add(expectedShoesSecond);

        //WHEN
        shoesDAO.addEntity(expectedShoesList);
        LinkedList<ShoesEntity> resultShoesList = shoesDAO.getEntities();

        //THEN
        assertEquals(expectedShoesList, resultShoesList);
    }

    @Test
    public void updateEntity() throws IOException, SQLException {
        //GIVEN
        ShoesEntity startShoes = new ShoesEntity(1, "Slaps", 250);
        startShoes.setShoesNumber(5);
        ShoesEntity expectedShoes = new ShoesEntity(1, "Boots", 1200);
        expectedShoes.setShoesNumber(5);

        //WHEN
        shoesDAO.addEntity(startShoes);
        shoesDAO.updateEntity(expectedShoes);
        ShoesEntity resultShoes = shoesDAO.getEntities().getFirst();

        //THEN
        assertEquals(expectedShoes, resultShoes);
    }

    @Test
    public void updateEntitiesList() throws IOException, SQLException {
        //GIVEN
        ShoesEntity startShoes1 = new ShoesEntity(1, "Slaps", 250);
        startShoes1.setShoesNumber(3);
        ShoesEntity expectedShoes1 = new ShoesEntity(1, "Slaps", 240);
        expectedShoes1.setShoesNumber(3);

        ShoesEntity startShoes2 = new ShoesEntity(1, "Sneakers", 400);
        startShoes2.setShoesNumber(4);
        ShoesEntity expectedShoes2 = new ShoesEntity(1, "Sleepers", 700);
        expectedShoes2.setShoesNumber(4);

        LinkedList<ShoesEntity> expectedShoesList = new LinkedList<>();
        expectedShoesList.add(expectedShoes1);
        expectedShoesList.add(expectedShoes2);

        //WHEN
        shoesDAO.addEntity(startShoes1);
        shoesDAO.addEntity(startShoes2);
        shoesDAO.updateEntity(expectedShoesList);
        LinkedList<ShoesEntity> resultShoesList = shoesDAO.getEntities();

        //THEN
        assertEquals(expectedShoesList, resultShoesList);
    }

    @Test
    public void deleteEntity() throws IOException, SQLException {
        //GIVEN
        ShoesEntity startShoes1 = new ShoesEntity(1, "Slaps", 250);
        startShoes1.setShoesNumber(1);
        ShoesEntity startShoes2 = new ShoesEntity(1, "Boots", 1200);
        startShoes2.setShoesNumber(2);

        LinkedList<ShoesEntity> expectedShoesList = new LinkedList<>();
        expectedShoesList.add(startShoes1);

        //WHEN
        shoesDAO.addEntity(startShoes1);
        shoesDAO.addEntity(startShoes2);
        shoesDAO.deleteEntity(2);
        LinkedList<ShoesEntity> resultShoesList = shoesDAO.getEntities();

        //THEN
        assertEquals(expectedShoesList, resultShoesList);
    }
}