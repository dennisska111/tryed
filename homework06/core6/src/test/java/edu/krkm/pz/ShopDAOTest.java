package edu.krkm.pz;

import org.junit.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;

import static org.junit.Assert.*;

public class ShopDAOTest{

    private ShopDAO shopDAO = new ShopDAO();

    @After
    public void clearTableShop() throws IOException, SQLException {
        for(ShopEntity shop: shopDAO.getEntities()) {
            shopDAO.deleteEntity(shop.getNumber());
        }
    }

    @Test
    public void addEntity() throws IOException, SQLException {
        //GIVEN
        ShopEntity expectedShop = new ShopEntity("Varus", "Lvov");
        expectedShop.setNumber(7);

        //WHEN
        shopDAO.addEntity(expectedShop);
        ShopEntity resultShop = shopDAO.getEntities().getFirst();

        //THEN
        assertEquals(expectedShop, resultShop);
    }

    @Test
    public void addEntitiesList() throws IOException, SQLException {
        //GIVEN
        ShopEntity expectedShopFirst = new ShopEntity("Varus", "Lvov");
        expectedShopFirst.setNumber(8);
        ShopEntity expectedShopSecond = new ShopEntity("ATB", "Kiev");
        expectedShopSecond.setNumber(9);
        LinkedList<ShopEntity> expectedShopsList = new LinkedList<>();
        expectedShopsList.add(expectedShopFirst);
        expectedShopsList.add(expectedShopSecond);

        //WHEN
        shopDAO.addEntity(expectedShopsList);
        LinkedList<ShopEntity> resultShopsList = shopDAO.getEntities();

        //THEN
        assertEquals(expectedShopsList, resultShopsList);
    }

    @Test
    public void updateEntity() throws IOException, SQLException {
        //GIVEN
        ShopEntity startShop = new ShopEntity("ATB", "Dnipro");
        startShop.setNumber(6);
        ShopEntity expectedShop = new ShopEntity("ATB", "Kharkiv");
        expectedShop.setNumber(6);

        //WHEN
        shopDAO.addEntity(startShop);
        shopDAO.updateEntity(expectedShop);
        ShopEntity resultShop = shopDAO.getEntities().getFirst();

        //THEN
        assertEquals(expectedShop, resultShop);
    }

    @Test
    public void updateEntitiesList() throws IOException, SQLException {
        //GIVEN
        ShopEntity startShop1 = new ShopEntity("ATB", "Dnipro");
        startShop1.setNumber(4);
        ShopEntity expectedShop1 = new ShopEntity("ATB", "Kharkiv");
        expectedShop1.setNumber(4);

        ShopEntity startShop2 = new ShopEntity("Varus", "Lvov");
        startShop2.setNumber(5);
        ShopEntity expectedShop2 = new ShopEntity("Fransua", "Lvov");
        expectedShop2.setNumber(5);

        LinkedList<ShopEntity> expectedShopsList = new LinkedList<>();
        expectedShopsList.add(expectedShop1);
        expectedShopsList.add(expectedShop2);

        //WHEN
        shopDAO.addEntity(startShop1);
        shopDAO.addEntity(startShop2);
        shopDAO.updateEntity(expectedShopsList);
        LinkedList<ShopEntity> resultShopsList = shopDAO.getEntities();

        //THEN
        assertEquals(expectedShopsList, resultShopsList);
    }

    @Test
    public void deleteEntity() throws IOException, SQLException {
        //GIVEN
        ShopEntity startShop1 = new ShopEntity("Fransua", "Odessa");
        startShop1.setNumber(1);
        ShopEntity startShop2 = new ShopEntity("Ashan", "Chernigov");
        startShop2.setNumber(2);

        LinkedList<ShopEntity> expectedShopsList = new LinkedList<>();
        expectedShopsList.add(startShop1);

        //WHEN
        shopDAO.addEntity(startShop1);
        shopDAO.addEntity(startShop2);
        shopDAO.deleteEntity(2);
        LinkedList<ShopEntity> resultShopsList = shopDAO.getEntities();

        //THEN
        assertEquals(expectedShopsList, resultShopsList);
    }
}