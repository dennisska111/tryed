package edu.krkm.pz;

import java.util.Objects;

public class ShoesEntity {
    private int shoesNumber;
    private int shopNumber;
    private String name;
    private int price;

    public ShoesEntity() {
        shoesNumber = 0;
        shopNumber = 0;
        name = "";
        price = 0;
    }

    public ShoesEntity(int shopNumber,
                       String name,
                       int price) {
        this.shoesNumber = 0;
        this.shopNumber = shopNumber;
        this.name = name;
        this.price = price;
    }

    @Override
    public String toString() {
        return "ShoesEntity{" +
                "shopNumber=" + shopNumber +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShoesEntity that = (ShoesEntity) o;
        return shopNumber == that.shopNumber &&
                Integer.compare(that.price, price) == 0 &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(shoesNumber, shopNumber, name, price);
    }

    void setShoesNumber(int shoesNumber) { this.shoesNumber = shoesNumber; }

    void setShopNumber(int shopNumber) { this.shopNumber = shopNumber; }

    void setName(String name) {
        this.name = name;
    }

    void setPrice(int price) {
        this.price = price;
    }

    int getShoesNumber() {
        return shoesNumber;
    }

    int getShopNumber() {
        return shopNumber;
    }

    String getName() {
        return name;
    }

    int getPrice() {
        return price;
    }
}
