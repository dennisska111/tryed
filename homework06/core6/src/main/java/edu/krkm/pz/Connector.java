package edu.krkm.pz;

import org.mariadb.jdbc.Driver;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class Connector {
    public static Connection getConnection() throws SQLException, IOException {
        Properties props = new Properties();

        try (InputStream in = Connector
                .class
                .getClassLoader()
                .getResourceAsStream(
                        "db/db.properties")) {
            props.load(in);
        }
        String drivers = props.getProperty("driver");
        if(drivers != null) {
            System.setProperty("driver", drivers);
        }

        String url = props.getProperty("url");
        String username = props.getProperty("username");
        String password = props.getProperty("password");

        return DriverManager.getConnection(url, username, password);
    }
}
