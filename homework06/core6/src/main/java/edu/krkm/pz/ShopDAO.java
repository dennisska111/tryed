package edu.krkm.pz;

import java.io.IOException;
import java.sql.*;
import java.util.LinkedList;

public class ShopDAO implements DAOInterface<ShopEntity>{
    @Override
    public LinkedList<ShopEntity> getEntities() throws SQLException, IOException {
        try(Connection con = Connector.getConnection();
            Statement stat = con.createStatement()) {
            LinkedList<ShopEntity> entitiesList = new LinkedList<>();
            try (ResultSet result = stat.executeQuery("SELECT * FROM shop")) {
                while (result.next()) {
                    ShopEntity shop = new ShopEntity(result.getString(2),
                            result.getString(3));
                    shop.setNumber(result.getInt(1));
                    entitiesList.add(shop);
                }
                return entitiesList;
            }
        }
    }

    @Override
    public void addEntity(LinkedList<ShopEntity> entities) throws SQLException, IOException {
        try(Connection con = Connector.getConnection()) {
            boolean autocommit = con.getAutoCommit();
            con.setAutoCommit(false);
            String insertQuery = "INSERT INTO shop VALUES (NULL, ?, ?)";
            PreparedStatement stat = con.prepareStatement(insertQuery);
            for(ShopEntity entity: entities) {
                stat.setString(1, entity.getName());
                stat.setString(2, entity.getAddress());
                stat.addBatch();
            }
            stat.executeBatch();
            con.commit();
            con.setAutoCommit(autocommit);
        }
    }

    @Override
    public void addEntity(ShopEntity entity) throws SQLException, IOException {
        try(Connection con = Connector.getConnection()) {
            PreparedStatement stat;
            if(entity.getNumber() == 0) {
                String insertQuery = "INSERT INTO shop VALUES (NULL, ?, ?);";
                stat = con.prepareStatement(insertQuery);
                stat.setString(1, entity.getName());
                stat.setString(2, entity.getAddress());
            }
            else {
                String insertQuery = "INSERT INTO shop VALUES (?, ?, ?);";
                stat = con.prepareStatement(insertQuery);
                stat.setInt(1, entity.getNumber());
                stat.setString(2, entity.getName());
                stat.setString(3, entity.getAddress());
            }
            stat.executeUpdate();
        }
    }

    @Override
    public void updateEntity(LinkedList<ShopEntity> entities) throws SQLException, IOException {
        try(Connection con = Connector.getConnection()) {
            for(ShopEntity entity: entities) {
                updateEntity(entity);
            }
        }
    }

    @Override
    public void updateEntity(ShopEntity entity) throws SQLException, IOException {
        try(Connection con = Connector.getConnection()) {
            String updateQuery = "UPDATE shop SET name = ?, address = ? WHERE id = ?";
            PreparedStatement stat = con.prepareStatement(updateQuery);
            stat.setInt(3, entity.getNumber());
            stat.setString(1, entity.getName());
            stat.setString(2, entity.getAddress());
            if(stat.executeUpdate() == 0) {
                addEntity(entity);
            }
        }
    }

    @Override
    public boolean deleteEntity(int id) throws SQLException, IOException {
        try(Connection con = Connector.getConnection()) {
            String deleteQuery = "DELETE FROM shop WHERE id = ?";
            PreparedStatement stat = con.prepareStatement(deleteQuery);
            stat.setInt(1, id);
            return stat.executeUpdate() != 0;
        }
    }
}
