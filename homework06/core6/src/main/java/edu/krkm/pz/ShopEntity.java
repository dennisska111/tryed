package edu.krkm.pz;

import java.util.Objects;

public class ShopEntity {
    private int number;
    private String name;
    private String address;

    public ShopEntity() {
        number = 0;
        name = "";
        address = "";
    }

    public ShopEntity(String name,
                      String address) {
        this.number = 0;
        this.name = name;
        this.address = address;
    }

    @Override
    public String toString() {
        return "ShopEntity{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShopEntity entity = (ShopEntity) o;
        return  Objects.equals(name, entity.name) &&
                Objects.equals(address, entity.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, name, address);
    }

    void setNumber(int number) {
        this.number = number;
    }

    void setName(String name) {
        this.name = name;
    }

    void setAddress(String address) {
        this.address = address;
    }

    int getNumber() {
        return number;
    }

    String getName() {
        return name;
    }

    String getAddress() {
        return address;
    }
}
