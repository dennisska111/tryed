package edu.krkm.pz;

import java.io.IOException;
import java.sql.*;
import java.util.LinkedList;

public class ShoesDAO implements DAOInterface<ShoesEntity> {
    @Override
    public LinkedList<ShoesEntity> getEntities() throws SQLException, IOException {
        try(Connection con = Connector.getConnection();
            Statement stat = con.createStatement()) {
            LinkedList<ShoesEntity> entitiesList = new LinkedList<>();
            try (ResultSet result = stat.executeQuery("SELECT * FROM shoes")) {
                while (result.next()) {
                    ShoesEntity product = new ShoesEntity(result.getInt(2),
                            result.getString(3),
                            result.getInt(4));
                    product.setShoesNumber(result.getInt(1));
                    entitiesList.add(product);
                }
                return entitiesList;
            }
        }
    }

    @Override
    public void addEntity(LinkedList<ShoesEntity> entities) throws SQLException, IOException {
        try(Connection con = Connector.getConnection()) {
            boolean autocommit = con.getAutoCommit();
            con.setAutoCommit(false);
            String insertQuery = "INSERT INTO shoes VALUES (NULL, ?, ?, ?)";
            PreparedStatement stat = con.prepareStatement(insertQuery);
            for(ShoesEntity entity: entities) {
                stat.setInt(1, entity.getShopNumber());
                stat.setString(2, entity.getName());
                stat.setInt(3, entity.getPrice());
                stat.addBatch();
            }
            stat.executeBatch();
            con.commit();
            con.setAutoCommit(autocommit);
        }
    }

    @Override
    public void addEntity(ShoesEntity entity) throws SQLException, IOException {
        try(Connection con = Connector.getConnection()) {
            PreparedStatement stat;
            if(entity.getShoesNumber() == 0) {
                String insertQuery = "INSERT INTO shoes VALUES (NULL, ?, ?, ?)";
                stat = con.prepareStatement(insertQuery);
                stat.setInt(1, entity.getShopNumber());
                stat.setString(2, entity.getName());
                stat.setInt(3, entity.getPrice());
            }
            else {
                String insertQuery = "INSERT INTO shoes VALUES (?, ?, ?, ?)";
                stat = con.prepareStatement(insertQuery);
                stat.setInt(1, entity.getShoesNumber());
                stat.setInt(2, entity.getShopNumber());
                stat.setString(3, entity.getName());
                stat.setInt(4, entity.getPrice());
            }
            stat.executeUpdate();
        }
    }

    @Override
    public void updateEntity(LinkedList<ShoesEntity> entities) throws SQLException, IOException {
        try(Connection con = Connector.getConnection()) {
            for(ShoesEntity entity: entities) {
                updateEntity(entity);
            }
        }
    }

    @Override
    public void updateEntity(ShoesEntity entity) throws SQLException, IOException {
        try(Connection con = Connector.getConnection()) {
            String updateQuery = "UPDATE shoes SET shop_id = ?, name = ?, price = ? WHERE id = ?";
            PreparedStatement stat = con.prepareStatement(updateQuery);
            stat.setInt(4, entity.getShoesNumber());
            stat.setInt(1, entity.getShopNumber());
            stat.setString(2, entity.getName());
            stat.setInt(3, entity.getPrice());
            if(stat.executeUpdate() == 0) {
                addEntity(entity);
            }
        }
    }

    @Override
    public boolean deleteEntity(int id) throws SQLException, IOException {
        try(Connection con = Connector.getConnection()) {
            String deleteQuery = "DELETE FROM shoes WHERE id = ?";
            PreparedStatement stat = con.prepareStatement(deleteQuery);
            stat.setInt(1, id);
            return stat.executeUpdate() != 0;
        }
    }
}
