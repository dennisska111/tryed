package edu.krkm.pz;

import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;

public class App 
{
    public static void printShops(LinkedList<ShopEntity> listShops) {
        System.out.println("Список магазинов:");
        for(ShopEntity it: listShops) {
            System.out.print("Номер: " + it.getNumber());
            System.out.print(". Название: " + it.getName());
            System.out.println(". Адрес: " + it.getAddress());
        }
    }

    public static void printShoes(LinkedList<ShoesEntity> listShoes) {
        System.out.println("Список товаров:");
        for(ShoesEntity it: listShoes) {
            System.out.print("Номер товара: " + it.getShoesNumber());
            System.out.print(". Название: " + it.getName());
            System.out.print(". Номер магазина: " + it.getShopNumber());
            System.out.println(". Цена: " + it.getPrice());
        }
    }

    public static void demonstrateDB() throws IOException, SQLException {
        ShopDAO shopDAO = new ShopDAO();
        ShoesDAO shoesDAO = new ShoesDAO();

        try {
            System.out.println("Исходные данные:");
            LinkedList<ShopEntity> shopsList = shopDAO.getEntities();
            LinkedList<ShoesEntity> shoesList = shoesDAO.getEntities();
            printShops(shopsList);
            printShoes(shoesList);

            shopsList.getFirst().setName("Puma");
            shopsList.getFirst().setAddress("Lvov");
            shopDAO.updateEntity(shopsList);

            System.out.println("Обновили первый магазин:");
            printShops(shopDAO.getEntities());

            shoesList.clear();
            shoesList.add(new ShoesEntity(1, "Sleepers", 400));
            shoesList.add(new ShoesEntity(2, "Clogs", 525));
            shoesList.getFirst().setShoesNumber(1);
            shoesList.getLast().setShoesNumber(4);
            shoesDAO.updateEntity(shoesList);

            System.out.println("Обновили первую и последнюю обувь:");
            printShoes(shoesDAO.getEntities());

            shopDAO.deleteEntity(2);
            System.out.println("Удалили магазин под номером 2:");
            printShops(shopDAO.getEntities());
            printShoes(shoesDAO.getEntities());
        }
        catch(SQLException | IOException exc) {
            throw exc;
        }
    }

    public static void main( String[] args ) {
        try {
            demonstrateDB();
        }
        catch(SQLException | IOException exc) {
            System.out.println(exc);
        }
    }
}
