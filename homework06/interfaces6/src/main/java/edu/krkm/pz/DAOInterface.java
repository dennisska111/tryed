package edu.krkm.pz;

import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;

public interface DAOInterface<T> {
    LinkedList<T> getEntities() throws SQLException, IOException;

    void addEntity(LinkedList<T> entities) throws SQLException, IOException;
    void addEntity(T entity) throws SQLException, IOException;

    void updateEntity(LinkedList<T> entities) throws SQLException, IOException;
    void updateEntity(T entity) throws SQLException, IOException;

    boolean deleteEntity(int id) throws SQLException, IOException;
}
